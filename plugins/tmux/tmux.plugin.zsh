[[ -s $HOME/.tmuxinator/scripts/tmuxinator ]] && source $HOME/.tmuxinator/scripts/tmuxinator

_mux() {
  compadd `mux list|grep -v "tmuxinator" | sed -e 's/^[ \t]*//'`
}
compdef _mux mux

