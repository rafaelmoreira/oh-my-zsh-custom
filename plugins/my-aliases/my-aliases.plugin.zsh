alias ls='ls -lah'


bindkey -e
bindkey '^[[1;9C' forward-word
bindkey '^[[1;9D' backward-word
bindkey '\033[5D' beginning-of-line
bindkey '\033[5C' end-of-line
